# Finding Disease-Causing Mutations with Privacy Protection

This project provides a proof-of-concept implementation of several protocols for finding fisease-causing mutations with privacy protection. We use the [ABY](https://github.com/encryptogroup/ABY) library for secure two-party computation. 


### Building


1. Clone the git repository by running 

    ```
    git clone https://gitlab.com/DIFUTURE/privacy-preserving-genomic-diagnosis.git
    ```
    
2. Enter the directory

    ```
    cd privacy-preserving-genomic-diagnosis/
    ```
    
3. Run the following command to compile the code

    ```
    make
    ```
    
### Using the Code

We provide three different secure operations on genomic data

* `RECESSIVE`
* `DOMINANT`
* `COMPHET`
* `MAX`
* `SETDIFF`
* `INTERSECTION`

Our protocol implementation fills gene and variant vectors with random bits. You do not need to specify any input file. You can run the programs with the following set of commands:

* RECESSIVE
  * Server: `./bin/recessive -r 0 -v 20000 -a 4 -u 4 -o 4 -i <ip of the server>`
  * Client: `./bin/recessive -r 1 -v 20000 -a 4 -u 4 -o 4 -i <ip of the server>`
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-v [Number of Variants, required]`
    *  `-a [Number of Affected Siblings, required]`
    *  `-u [Number of Unaffected Siblings, required]`
    *  `-o [Number of Others, required]`
    *  `-i [IP-address, default: localhost, optional]`
* DOMINANT
  * Server: `./bin/dominant -r 0 -v 20000 -a 4 -u 4 -o 4 -i <ip of the server>`
  * Client: `./bin/dominant -r 1 -v 20000 -a 4 -u 4 -o 4 -i <ip of the server>`
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-v [Number of Variants, required]`
    *  `-a [Number of Affected Siblings, required]`
    *  `-u [Number of Unaffected Siblings, required]`
    *  `-o [Number of Others, required]`
    *  `-i [IP-address, default: localhost, optional]`
* COMPHET
  * Server: `./bin/comphet -r 0 -v 20000 -a 4 -u 4 -o 4 -i <ip of the server>`
  * Client: `./bin/comphet -r 1 -v 20000 -a 4 -u 4 -o 4 -i <ip of the server>`
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-v [Number of Variants, required]`
    *  `-a [Number of Affected Siblings, required]`
    *  `-u [Number of Unaffected Siblings, required]`
    *  `-o [Number of Others, required]`
    *  `-i [IP-address, default: localhost, optional]`
* MAX
  * Server: `./bin/max -r 0 -v 20000 -h 4 -a <ip of the server>`
  * Client: `./bin/max -r 1 -v 20000 -h 4 -a <ip of the server>`
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-v [Number of Genes, required]`
    *  `-h [Number of Patients, required]`
    *  `-a [IP-address, default: localhost, optional]`
* SETDIFF
  * Server: `./bin/setdiff -r 0 -v 1000000 -h 1 -a <ip of the server>`
  * Client: `./bin/setdiff -r 1 -v 1000000 -h 1 -a <ip of the server>`
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-v [Number of Variants, required]`
    *  `-h [Number of Patients, required]`
    *  `-a [IP-address, default: localhost, optional]`
* INTERSECTION
  * Server: `./bin/intersection -r 0 -v 1000000 -h 2 -a <ip of the server>`
  * Client: `./bin/intersection -r 1 -v 1000000 -h 2 -a <ip of the server>`
  * Parameters: 
    *  `-r [Role: 0/1, required]`
    *  `-v [Number of Variants, required]`
    *  `-h [Number of Patients, required]`
    *  `-a [IP-address, default: localhost, optional]`
