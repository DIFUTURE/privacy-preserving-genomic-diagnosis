#include "comphet.h"

double o_time  = 0;
double os_time  = 0, o_comm = 0, s_comm = 0;

int32_t test_comphet(e_role role, char* address, uint16_t port, seclvl seclvl,
		uint32_t nthreads, e_mt_gen_alg mt_alg,
		e_sharing sharing,uint32_t nvariant,uint32_t naffec, uint32_t nunaff, uint32_t nothers) {




	// p1 is arithmetically shared 
	// if p1[i] == 0 then p1'[i]^p1''[i]=0
	// we use this fact we dont convert shares to bool shares

	srand(100);
	
	uint16_t ngene=1000;
	uint16_t glen=nvariant/ngene;
	uint32_t* mother = new uint32_t[nvariant];
	uint32_t* father = new uint32_t[nvariant];
	uint32_t* affected_siblings = new uint32_t[nvariant];
	uint32_t* unaffected_siblings = new uint32_t[nvariant];
	uint32_t* others = new uint32_t[nvariant];
	uint64_t zero = 0; 
	uint32_t total_1 = 1 + naffec;
        uint32_t total_2 = nunaff + nothers;
        uint32_t bitlen_1 = (uint16_t)(floor(log2((double)total_1)+1));
        uint32_t bitlen_2 = (uint16_t)(floor(log2((double)total_2)+1));
        uint32_t max_1 = (1<<bitlen_1);
        uint32_t max_2 = (1<<bitlen_2);
        uint32_t tmp1,tmp2;

	for (int i=0;i<nvariant;i++){
  		tmp1 = rand()%2;
                tmp2 = rand()%max_1;
                if (role==SERVER)
                        mother[i]=tmp2;
                else
                        mother[i]=((tmp1+max_1)-tmp2)%max_1;

                tmp1 = rand()%2;
                tmp2 = rand()%max_1;
                if (role==SERVER)
                        father[i]=tmp2;
                else
                        father[i]=((tmp1+max_1)-tmp2)%max_1;

                tmp1 = rand()%(naffec+1);
                tmp2 = rand()%max_1;
                if (role==SERVER)
                        affected_siblings[i]=tmp2;
                else
                        affected_siblings[i]=((tmp1+max_1)-tmp2)%max_1;

 		tmp1 = rand()%(nunaff+1);
                tmp2 = rand()%max_2;
                if (role==SERVER)
                        unaffected_siblings[i]=tmp2;
                else
                        unaffected_siblings[i]=((tmp1+max_2)-tmp2)%max_2;

                tmp1 = rand()%(nothers+1);
                tmp2 = rand()%max_2;
                if (role==SERVER)
                        others[i]=tmp2;
                else
                        others[i]=((tmp1+max_2)-tmp2)%max_2;

	}


	for (int i=0;i<nvariant;i++){
		mother[i] = (mother[i] + affected_siblings[i])%max_1;
		father[i] = (father[i] + affected_siblings[i])%max_1;
		others[i] = (others[i] + unaffected_siblings[i])%max_2; // gives the locations on which all unaffected siblings and others have non-heterozygous variants; the locations are marked with zero
		if (role == SERVER){
			mother[i] = (mother[i] + max_1 - total_1)%max_1; // gives the locations on which all affected siblings and the mother have non-heterozygous variants; the locations are marked with zero
                	father[i] = (father[i] + max_1 - total_1)%max_1; // gives the locations on which all affected siblings and the father have non-heterozygous variants; the locations are marked with zero

			mother[i] = (max_1 - mother[i]); // a2b_l local conversion of zero values 
			father[i] = (max_1 - father[i]); // a2b_l local conversion of zero values
			others[i] = (max_2 - others[i]); // a2b_l local conversion of zero values
		}

	}

	uint32_t bitlen = bitlen_1;
	if (bitlen_2>bitlen)
		bitlen=bitlen_2;
	ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
			mt_alg);

	vector<Sharing*>& sharings = party->GetSharings();

       	BooleanCircuit* circ = (BooleanCircuit*)sharings[S_BOOL]->GetCircuitBuildRoutine();

        share *s_1,*s_2,*s_3,*zeros;

	s_1 = circ->PutSharedSIMDINGate(nvariant,mother,bitlen_1);
        s_2 = circ->PutSharedSIMDINGate(nvariant,father,bitlen_1);
	s_3 = circ->PutSharedSIMDINGate(nvariant,others,bitlen_2);
        zeros = circ->PutSIMDCONSGate(nvariant,zero,1);

        s_1 = circ->PutEQGate(s_1,zeros);
	s_2 = circ->PutEQGate(s_2,zeros);
	s_3 = circ->PutEQGate(s_3,zeros);

	s_1 = circ->PutANDGate(s_1,s_3);
	s_2 = circ->PutANDGate(s_2,s_3);

	share* is_1 = circ->PutINVGate(s_1);
	share* is_2 = circ->PutINVGate(s_2);
	s_1 = circ->PutANDGate(s_1,is_2);
	s_2 = circ->PutANDGate(s_2,is_1);


	share* sum_m = circ->PutCONSGate(zero,ngene);
	share* sum_f = circ->PutCONSGate(zero,ngene);

	uint32_t* ps = new uint32_t[nvariant];

	for (int i = 0; i<ngene; i++) {
        	uint32_t pos[glen];
        	for (int j=0; j<glen; j++) {
            		pos[j] = (i*glen)+j;
	    		if (pos[j]>=nvariant)
				pos[j]=nvariant-1;
			ps[pos[j]]=i;
        	}
        	share* part_m = circ->PutSubsetGate(s_1,pos,glen);
		share* part_f = circ->PutSubsetGate(s_2,pos,glen);
        	uint32_t sz = 1;
		while(sz<glen)
			sz=(sz<<1);
		sz=(sz>>1);
		uint32_t pos1[sz];
        	uint32_t pos2[sz];
        	for (int j=0; j<sz; j++) {
                	pos1[j] = j;
                	pos2[j] = sz + j;
			if (pos2[j] >=glen)
				pos2[j]=glen-1;
        	}
		share* part1 = circ->PutSubsetGate(part_m,pos1,sz);
        	share* part2 = circ->PutSubsetGate(part_m,pos2,sz);
        	part_m = circ->PutORGate(part1,part2);
		part1 = circ->PutSubsetGate(part_f,pos1,sz);
                part2 = circ->PutSubsetGate(part_f,pos2,sz);
                part_f = circ->PutORGate(part1,part2);


        	while (1){
            		if (sz == 1)
                		break;
            		uint32_t part_sz = (sz>>1);

           		uint32_t pos1[part_sz];
            		uint32_t pos2[part_sz];
            		for (int j=0; j<part_sz; j++) {
               	 		pos1[j] = j;
                		pos2[j] = part_sz + j;
            		}
            		share* part1 = circ->PutSubsetGate(part_m,pos1,part_sz);
            		share* part2 = circ->PutSubsetGate(part_m,pos2,part_sz);
            		part_m = circ->PutORGate(part1,part2);
			part1 = circ->PutSubsetGate(part_f,pos1,part_sz);
                        part2 = circ->PutSubsetGate(part_f,pos2,part_sz);
                        part_f = circ->PutORGate(part1,part2);

            		sz = part_sz;

        	}


        	sum_m->set_wire_id(i,part_m->get_wire_id(0));
		sum_f->set_wire_id(i,part_f->get_wire_id(0));
    	}

	sum_m = circ->PutCombinerGate(sum_m);
	sum_m = circ->PutSubsetGate(sum_m,ps,nvariant);
	sum_f = circ->PutCombinerGate(sum_f);
        sum_f = circ->PutSubsetGate(sum_f,ps,nvariant);

	s_1 = circ->PutANDGate(s_1,sum_f);
        s_2 = circ->PutANDGate(s_2,sum_m);

	delete [] ps;

	share *s_out = circ->PutOUTGate(s_1,ALL);
	share *s_out1 = circ->PutOUTGate(s_2,ALL);


        party->ExecCircuit();

 	uint32_t out_bitlen , out_nvals , *out_vals;
        s_out->get_clear_value_vec(&out_vals, &out_bitlen, &out_nvals);

	uint32_t count = 0;
        for (uint32_t i = 0;i<nvariant;i++){
                if (out_vals[i] == 1){
                        count++;
                }
        }
        cout<<"Number of Comphet Variants from mother:"<<count<<endl;


	s_out1->get_clear_value_vec(&out_vals, &out_bitlen, &out_nvals);

        count = 0;
        for (uint32_t i = 0;i<nvariant;i++){
                if (out_vals[i] == 1){
                        count++;
                }
        }
        cout<<"Number of Comphet Variants from father:"<<count<<endl;

	
	delete [] mother;
	delete [] father;
	delete [] unaffected_siblings;
	delete [] affected_siblings;
	delete [] others;
	delete party;
	return 0;
}



