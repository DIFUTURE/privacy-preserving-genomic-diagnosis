#include "setdiff.h"

double o_time  = 0;
double os_time  = 0, o_comm = 0, s_comm = 0;

int32_t test_setdiff(e_role role, char* address, uint16_t port, seclvl seclvl,
		uint32_t nthreads, e_mt_gen_alg mt_alg,
		e_sharing sharing,uint32_t nvariant,uint32_t npatient) {




	// p1 is arithmetically shared 
	// if p1[i] == 0 then p1'[i]^p1''[i]=0
	// we use this fact we dont convert shares to bool shares

	srand(time(NULL));
	
	
	uint32_t* p1 = new uint32_t[nvariant];
	uint32_t* p2 = new uint32_t[nvariant];
	uint64_t zero = 0; 
	uint32_t bitlen = (uint16_t)(floor(log2((double)npatient))+1);

	for (int i=0;i<nvariant;i++){
		p1[i] = rand()%(npatient+1);
		p2[i] = rand()%2;
	}

	
	ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
			mt_alg);

	vector<Sharing*>& sharings = party->GetSharings();

       	Circuit* circ = sharings[S_BOOL]->GetCircuitBuildRoutine();

        share *s_1,*s_2,*zeros;

	s_1 = circ->PutSharedSIMDINGate(nvariant,p1,bitlen);
        s_2 = circ->PutSharedSIMDINGate(nvariant,p2,1);
        zeros = circ->PutSIMDCONSGate(nvariant,zero,bitlen);

        share* eq = circ->PutEQGate(s_1,zeros);

        share* out = circ->PutANDGate(eq,s_2);

	share *s_out = circ->PutOUTGate(out,ALL);


        party->ExecCircuit();

 	uint32_t out_bitlen , out_nvals , *out_vals;
        s_out->get_clear_value_vec(&out_vals, &out_bitlen, &out_nvals);

	uint32_t diff_count = 0;
        for (uint32_t i = 0;i<nvariant;i++){
                if (out_vals[i] == 1){
                        diff_count++;
                }
        }
        cout<<"Number of Different Variants :"<<diff_count<<endl;

	
	delete [] p1;
	delete [] p2;
	delete party;
	return 0;
}



