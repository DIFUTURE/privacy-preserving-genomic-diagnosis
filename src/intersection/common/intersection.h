
#ifndef __INTERSECTION_H_
#define __INTERSECTION_H_

#include "../../../abycore/circuit/booleancircuits.h"
#include "../../../abycore/circuit/arithmeticcircuits.h"
#include "../../../abycore/circuit/circuit.h"
#include "../../../abycore/aby/abyparty.h"
#include <math.h>
#include <cassert>



int32_t test_intersection(e_role role, char* address, uint16_t port, seclvl seclvl,
		uint32_t nthreads, e_mt_gen_alg mt_alg,
		e_sharing sharing,uint32_t nvariant,uint32_t npatient);


#endif /* __INTERSECTION_H_ */
