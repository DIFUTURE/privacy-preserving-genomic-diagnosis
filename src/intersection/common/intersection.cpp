
#include "intersection.h"


int32_t test_intersection(e_role role, char* address, uint16_t port, seclvl seclvl,
		uint32_t nthreads, e_mt_gen_alg mt_alg,
		e_sharing sharing,uint32_t nvariant,uint32_t npatient) {

	srand(time(NULL));
	
	
	uint32_t* p1 = new uint32_t[nvariant];
	uint32_t* t = new uint32_t[nvariant];

	uint32_t bitlen = (uint16_t)(floor(log2((double)npatient))+1);
	
	for (uint32_t i=0;i<nvariant;i++){
		p1[i] = rand()%(npatient+1);
		t[i] = 0;
	}

	ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
			mt_alg);

	double o_time  = 0;
        double os_time  = 0, o_comm = 0, s_comm = 0;


        vector<Sharing*>& sharings = party->GetSharings();
        Circuit* circ = sharings[S_BOOL]->GetCircuitBuildRoutine();
	share *s_1,*s_t,*s_m,*s_out;


	s_1 = circ->PutSharedSIMDINGate(nvariant,p1,bitlen);
	s_t = circ->PutSharedSIMDINGate(nvariant,t,bitlen);




	share* s_res = circ->PutEQGate(s_1,s_t);
	share* tmp = circ->PutOUTGate(s_res , ALL);
        party->ExecCircuit();

        uint32_t out_bitlen , out_nvals , *out_vals;
        tmp->get_clear_value_vec(&out_vals, &out_bitlen, &out_nvals);

	uint32_t intersection_count = 0;
	for (uint32_t i = 0;i<nvariant;i++){
		if (out_vals[i] == 1){
			intersection_count++;
		}
	}
	cout<<"Number of Common Variants :"<<intersection_count<<endl;

        
	delete [] p1;
	delete [] t;
	delete party;
	return 0;
}

