#include "dominant.h"

double o_time  = 0;
double os_time  = 0, o_comm = 0, s_comm = 0;

int32_t test_dominant(e_role role, char* address, uint16_t port, seclvl seclvl,
		uint32_t nthreads, e_mt_gen_alg mt_alg,
		e_sharing sharing,uint32_t nvariant,uint32_t naffec, uint32_t nunaff, uint32_t nothers) {




	// p1 is arithmetically shared 
	// if p1[i] == 0 then p1'[i]^p1''[i]=0
	// we use this fact we dont convert shares to bool shares

	srand(100);
	
	uint32_t* affected_siblings = new uint32_t[nvariant];
	uint32_t* unaffected_siblings_h = new uint32_t[nvariant];
	uint32_t* unaffected_siblings_e = new uint32_t[nvariant];
	uint32_t* others_h = new uint32_t[nvariant];
	uint32_t* others_e = new uint32_t[nvariant];
	uint64_t zero = 0;
        uint32_t total_1 = naffec; 
	uint32_t total_2 = nunaff + nothers;
	uint32_t bitlen_1 = (uint16_t)(floor(log2((double)total_1)+1));
	uint32_t bitlen_2 = (uint16_t)(floor(log2((double)total_2)+1));
	uint32_t max_1 = (1<<bitlen_1);
	uint32_t max_2 = (1<<bitlen_2);
	uint32_t tmp1,tmp2,tmp3;

	for (int i=0;i<nvariant;i++){
                tmp1 = rand()%(naffec+1);
                tmp2 = rand()%max_1;
                if (role==SERVER)
                        affected_siblings[i]=tmp2;
                else
                        affected_siblings[i]=((tmp1+max_1)-tmp2)%max_1;

                tmp3 = rand()%(nunaff+1);
		tmp1 = rand()%(tmp3+1);
                tmp2 = rand()%max_2;
                if (role==SERVER)
                        unaffected_siblings_h[i]=tmp2;
                else
                        unaffected_siblings_h[i]=((tmp1+max_2)-tmp2)%max_2;

		tmp1 = tmp3-tmp1;
                tmp2 = rand()%max_2;
                if (role==SERVER)
                        unaffected_siblings_e[i]=tmp2;
                else
                        unaffected_siblings_e[i]=((tmp1+max_2)-tmp2)%max_2;



                tmp3 = rand()%(nothers+1);
		tmp1 = rand()%(tmp3+1);
                tmp2 = rand()%max_2;
                if (role==SERVER)
                       	others_h[i]=tmp2;
                else
                        others_h[i]=((tmp1+max_2)-tmp2)%max_2;

		tmp1 = tmp3-tmp1;
                tmp2 = rand()%max_2;
                if (role==SERVER)
                        others_e[i]=tmp2;
                else
                        others_e[i]=((tmp1+max_2)-tmp2)%max_2;


	}


	for (int i=0;i<nvariant;i++){
		others_h[i] = (others_h[i] + others_e[i] + unaffected_siblings_h[i] + unaffected_siblings_e[i])%max_2;
		if (role == SERVER){
			affected_siblings[i] = (affected_siblings[i] + max_1 - naffec)%max_1; //  gives the locations on which all affected siblings have homozygous variants and the mother and the father have heterozygous variants, the locations are marked with zero
			affected_siblings[i] = (max_1 - affected_siblings[i]); // a2b_l local conversion of zero values
			others_h[i] = (max_2 - others_h[i]); // a2b_l local conversion of zero values

		}

	}

	
	uint32_t bitlen = bitlen_1;
	if (bitlen_2>bitlen)
		bitlen=bitlen_2;
	ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
			mt_alg);

	vector<Sharing*>& sharings = party->GetSharings();

       	BooleanCircuit* circ = (BooleanCircuit*)sharings[S_BOOL]->GetCircuitBuildRoutine();

        share *s_1,*s_2,*s_3,*zeros;

	s_1 = circ->PutSharedSIMDINGate(nvariant,affected_siblings,bitlen_1);
        s_2 = circ->PutSharedSIMDINGate(nvariant,others_h,bitlen_2);
        zeros = circ->PutSIMDCONSGate(nvariant,zero,1);

        s_1 = circ->PutEQGate(s_1,zeros);
	s_2 = circ->PutEQGate(s_2,zeros);

	s_1 = circ->PutANDGate(s_1,s_2);




	share *s_out = circ->PutOUTGate(s_1,ALL);


        party->ExecCircuit();

 	uint32_t out_bitlen , out_nvals , *out_vals;
        s_out->get_clear_value_vec(&out_vals, &out_bitlen, &out_nvals);

	uint32_t dominant_count = 0;
        for (uint32_t i = 0;i<nvariant;i++){
                if (out_vals[i] == 1){
                        dominant_count++;
                }
        }
        cout<<"Number of Dominant Variants :"<<dominant_count<<endl;

	
	delete [] unaffected_siblings_h;
	delete [] unaffected_siblings_e;
	delete [] affected_siblings;
	delete [] others_h;
	delete [] others_e;
	delete party;
	return 0;
}



