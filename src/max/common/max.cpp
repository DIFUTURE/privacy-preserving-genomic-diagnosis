
#include "max.h"

int32_t test_max(e_role role, char* address, uint16_t port, seclvl seclvl,
		uint32_t nthreads, e_mt_gen_alg mt_alg,
		e_sharing sharing,uint32_t ngenes,uint32_t npatient) {


	srand(time(NULL));
	uint32_t* p1 = new uint32_t[ngenes];

	uint32_t bitlen = 8;
	if (npatient > 255)
		bitlen = 16;
	if (npatient > 65535)
		bitlen = 32;

		for (int i=0;i<ngenes;i++)
			p1[i] = rand()%(npatient+1);
	
        
        
	ABYParty* party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
			mt_alg);

        double o_time  = 0;
	double os_time  = 0, o_comm = 0, s_comm = 0;


	vector<Sharing*>& sharings = party->GetSharings();

	Circuit* circ = sharings[S_ARITH]->GetCircuitBuildRoutine();
	Circuit* circy = sharings[S_YAO]->GetCircuitBuildRoutine();
	share *s_1, *s_out;

	if (role == SERVER)
		s_1 = circ->PutSIMDINGate(ngenes,p1,bitlen,SERVER);
	else 
		s_1 = circ->PutDummySIMDINGate(ngenes,bitlen);
	s_1 = circy->PutA2YGate(s_1);
	//s_1->set_bitlength(4);

	s_out = BuildMaxCircuit(s_1,(BooleanCircuit*) circy,ngenes);	

	share* tmp = circy->PutOUTGate(s_out , ALL);

        party->ExecCircuit();

	uint32_t out_bitlen , out_nvals , *out_vals;

       	tmp->get_clear_value_vec(&out_vals, &out_bitlen, &out_nvals);

        printf("MAX  : %d ",out_vals[0]);

        o_time += party->GetTiming(P_ONLINE);
	os_time += party->GetTiming(P_SETUP);
	o_comm += party->GetSentData(P_ONLINE);
	s_comm += party->GetSentData(P_SETUP);


        cout<<"Offline time :\t" << os_time << endl;
	cout<<"Online time :\t" << o_time << endl;
 	cout<<"Offline data :\t" << s_comm << endl;
	cout<<"Online data :\t" << o_comm << endl;
        
        delete [] p1;
	delete party;
	return 0;
}

share* BuildMaxCircuit(share *s, BooleanCircuit *bc, int ngenes) {
	uint32_t asz = ngenes;
	uint64_t co = -1 ;
            while (1){
                    uint32_t to = asz/2;
                    uint32_t pos1[to];
                    uint32_t pos2[to];
                        for (int j=0;j<to;j++){
                            pos1[j] = j;
                            pos2[j] = to+j;
                        }
                    share* part1 = bc->PutSubsetGate(s,pos1,to);
                    share* part2 = bc->PutSubsetGate(s,pos2,to);
                    s = bc->PutMUXGate(part1,part2,bc->PutGTGate(part1,part2));
                    asz = to;
                    if (asz == 1)
                        break;

            }
	return s;
}

