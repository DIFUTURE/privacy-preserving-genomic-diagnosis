#include "../../abycore/ENCRYPTO_utils/crypto/crypto.h"
#include "../../abycore/ENCRYPTO_utils/parse_options.h"
#include "../../abycore/aby/abyparty.h"

#include "common/recessive.h"

int32_t read_test_options(int32_t* argcp, char*** argvp, e_role* role,
	       	uint32_t* secparam, string* address,
		uint16_t* port,uint32_t* nvariant,uint32_t* naffec,uint32_t* nunaff,uint32_t* nothers) {

	uint32_t int_role = 0, int_port = 0, int_aff = 1, int_naf = 1;
	bool useffc = false;

	parsing_ctx options[] =
			{ { (void*) &int_role, T_NUM, "r", "Role: 0/1", true, false },
			  { (void*) nvariant, T_NUM, "v", "Number of Variants, default: 100000", false, false },
                          { (void*) &int_aff, T_NUM, "a", "Number of Affected Siblings, default: 1", false, false },
                          { (void*) &int_naf, T_NUM, "u", "Number of Unaffected Siblings, default: 1", false, false },
                          { (void*) nothers, T_NUM, "o", "Number of Others, default: 0", false, false },
			  { (void*) secparam, T_NUM, "s",
					"Symmetric Security Bits, default: 128", false, false }, {
					(void*) address, T_STR, "i",
					"IP-address, default: localhost", false, false }, {
					(void*) &int_port, T_NUM, "p", "Port, default: 7766", false,
					false } };

	if (!parse_options(argcp, argvp, options,
			sizeof(options) / sizeof(parsing_ctx))) {
		print_usage(*argvp[0], options, sizeof(options) / sizeof(parsing_ctx));
		cout << "Exiting" << endl;
		exit(0);
	}

	assert(int_role < 2);
	*role = (e_role) int_role;

	if (int_aff == 0){
                cout<<"Number of Affected Siblings must be greater than 0"<<endl;
                exit(0);
        }
        *naffec = (uint32_t) int_aff;

        if (int_naf == 0){
                cout<<"Number of Unaffected Siblings must be greater than 0"<<endl;
                exit(0);
        }
        *nunaff = (uint32_t) int_naf;

        

	if (int_port != 0) {
		assert(int_port < 1 << (sizeof(uint16_t) * 8));
		*port = (uint16_t) int_port;
	}

	//delete options;

	return 1;
}

int main(int argc, char** argv) {

	e_role role;
	uint32_t secparam = 128, nthreads = 1;
	uint16_t port = 7766;
	string address = "127.0.0.1";

	uint32_t nvariant = 100000;
	uint32_t naffec = 1;
	uint32_t nunaff = 1;
	uint32_t nothers = 0;
	e_mt_gen_alg mt_alg = MT_OT;

	read_test_options(&argc, &argv, &role, &secparam, &address,
			&port,&nvariant,&naffec,&nunaff,&nothers);

	seclvl seclvl = get_sec_lvl(secparam);

        
        test_recessive(role, (char*) address.c_str(), port, seclvl,nthreads, mt_alg, S_BOOL,nvariant,naffec,nunaff,nothers);

	return 0;
}

